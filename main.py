import api_wrapper
import requests
import _tkinter
import tkinter
from tkinter import *
from io import BytesIO
from PIL import Image, ImageTk
from math import floor

from datetime import datetime

tinder_token = open("token.txt").readline().strip()
window_x = 750
window_y = 1200
image_x  = 600

class Window(Frame):
    def __init__(self, master=None):
        Frame.__init__(self, master)   
        self.master = master

        self.api = api_wrapper.ApiWrapper(tinder_token)
        self.profiles = self.api.nearby_persons()
        self.profile_index = 0

        self.canvas = tkinter.Canvas(root, borderwidth=0)
        self.frame = tkinter.Frame(self.canvas)
        self.vsb = tkinter.Scrollbar(root, orient="vertical", command=self.canvas.yview)
        self.canvas.configure(yscrollcommand=self.vsb.set)

        self.vsb.pack(side="right", fill="y")
        self.canvas.pack(side="left", fill="both", expand=True)
        self.canvas.create_window((4,4), window=self.frame, anchor="nw", 
                                  tags="self.frame")

        self.frame.bind("<Configure>", self.onFrameConfigure)

        self.init_window()

    def onFrameConfigure(self, event):
        '''Reset the scroll region to encompass the inner frame'''
        self.canvas.configure(scrollregion=self.canvas.bbox("all"))

    def init_window(self):
        self.master.title("Pynder")

        self.pack(fill=BOTH, expand=1)

        likeButton = Button(self, text="Like", command=self.like)
        likeButton.grid(row = 1, column = 1)

        dislikeButton = Button(self, text="Dislike", command=self.dislike)
        dislikeButton.grid(row = 1, column = 2)

        self.update()

    def showImg(self):
        row = 7
        for image in self.profiles[self.profile_index].images:
            image_response = requests.get(image)
            img = Image.open(BytesIO(image_response.content))
            width, height = img.size
            #scalar = max(width / window_x, height / window_y)
            scalar = width / image_x
            img = img.resize((floor(width / scalar), floor(height / scalar)))

            render = ImageTk.PhotoImage(img)

            img = Label(self.frame, image=render)
            img.image = render
            img.grid(row = row, column = 1)
            row += 1

    def showText(self):
        Label(self.frame, text=self.profiles[self.profile_index].name, wraplength=image_x).grid(row = 2, column = 1)
        Label(self.frame, text=self.profiles[self.profile_index].bio, wraplength=image_x).grid(row = 3, column = 1)
        Label(self.frame, text=str(self.profiles[self.profile_index].distance) + " miles", wraplength=image_x).grid(row = 4, column = 1)
        Label(self.frame, text=self.profiles[self.profile_index].jobs, wraplength=image_x).grid(row = 5, column = 1)
        Label(self.frame, text=self.profiles[self.profile_index].schools, wraplength=image_x).grid(row = 6, column = 1)

    def client_exit(self):
        exit()

    def update(self):
        for widget in self.frame.winfo_children():
            widget.destroy()
        self.profile_index += 1
        if self.profile_index == len(self.profiles):
            print("Next set!")
            self.profiles = self.api.nearby_persons()
            self.profile_index = 0
        print(self.profiles[self.profile_index].id)
        self.showImg()
        self.showText()
        self.canvas.yview(MOVETO, 0)

    def like(self):
        self.profiles[self.profile_index].like()
        self.update()

    def dislike(self):
        self.profiles[self.profile_index].dislike()
        self.update()

root = Tk()
root.geometry(str(window_x) + "x" + str(window_y))
app = Window(root).pack(side="top", fill="both", expand=True)

root.mainloop()
