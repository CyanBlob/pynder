import datetime
import requests
from io import BytesIO
from PIL import Image
from geopy.geocoders import Nominatim

geolocator = Nominatim(user_agent="auto-tinder")

class Person(object):

    def __init__(self, raw_data, api):
        self._api = api

        data = raw_data["user"]
        self.id = data["_id"]
        self.name = data.get("name", "Unknown")

        self.bio = data.get("bio", "")
        self.distance = raw_data.get("distance_mi", 0)

        self.birth_date = datetime.datetime.strptime(data["birth_date"], '%Y-%m-%dT%H:%M:%S.%fZ') if data.get(
            "birth_date", False) else None
        self.gender = ["Male", "Female", "Unknown"][data.get("gender", 2)]

        #self.images = list(map(lambda photo: photo["url"], data.get("photos", [])))
        # lower quality images (from 0-3, highest to lowest)
        self.images = list(map(lambda photo: photo["processedFiles"][1]["url"], data.get("photos", [])))

        self.jobs = list(
            map(lambda job: {"title": job.get("title", {}).get("name"), "company": job.get("company", {}).get("name")}, data.get("jobs", [])))
        self.schools = list(map(lambda school: school["name"], data.get("schools", [])))

        if data.get("pos", False):
            self.location = geolocator.reverse(f'{data["pos"]["lat"]}, {data["pos"]["lon"]}')


    def __repr__(self):
        return f"{self.id}  -  {self.name} ({self.birth_date.strftime('%d.%m.%Y')})"

    def like(self):
        return self._api.like(self.id)

    def dislike(self):
        return self._api.dislike(self.id)

    def show(self):
        image_response = requests.get(self.images[0])
        img = Image.open(BytesIO(image_response.content))

        img.show()
